﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContosoUniversity.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        [EmailAddress]
        public string EmailAddress { get; set; } 
        [DataType(DataType.Date)]
        public DateTime EnrollmentDate { get; set; }
        public virtual Collection<Enrollment> Enrollments { get; set; }

    }
}