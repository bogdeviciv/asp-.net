﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LoginProject.Login" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
        <div>
            Username:
            <asp:TextBox ID="tbUsername" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="tbUsername" ForeColor="Red" ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please enter the username!"></asp:RequiredFieldValidator>
            <br />
            Password:
            <asp:TextBox ID="tbPassword" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ControlToValidate="tbPassword" ForeColor="Red" ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please enter the password!"></asp:RequiredFieldValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="error" ForeColor="Red" runat="server" ></asp:Label>
        </div>
   
</asp:Content>