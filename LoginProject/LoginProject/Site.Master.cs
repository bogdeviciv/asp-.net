﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LoginProject
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["user"]!= null)
            {
                Login.InnerText = "Logoff";
            }
            else Login.InnerText = "Login";
            
        }

        protected void Login_ServerClick(object sender, EventArgs e)
        {
            if (Session["user"] != null)
            {
                Session.Remove("user");
                
            }
            Response.Redirect("Login.aspx");
        }

        protected void Unnamed_ServerClick(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("Login.aspx");

            }
            HtmlAnchor link = (HtmlAnchor)sender;
            Response.Redirect(link.HRef);
        }
    }
}