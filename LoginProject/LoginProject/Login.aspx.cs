﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginProject
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string usernameInp = tbUsername.Text;
            string passwordInp = tbPassword.Text;
            if(usernameInp.Equals(Global.username) && passwordInp.Equals(Global.password))
            {
                Session["user"] = usernameInp;
                Response.Redirect("Default.aspx");
            }
            else
            {
                error.Text = "Username/password info not match";
            }
        }
    }
}