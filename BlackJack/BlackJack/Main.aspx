﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="BlackJack.Main" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <link href="styles.css" rel="stylesheet" />
    
    <title></title>
</head>
<body>
    <form id="Mycontainer" runat="server">
    

    
    <div id="dealer" style="height: 30%">
    <h3>Dealer</h3>
        <asp:Label ID="dealerPoints" runat="server"><%%></asp:Label>
    <div class="Myimages">       
    <asp:PlaceHolder ID="dealerHand" runat="server"></asp:PlaceHolder>
        
        </div>
    </div>
    <div id="player" style="height: 70%">
       
        <div class="Myimages">
        <asp:PlaceHolder ID="playerHand" runat="server"></asp:PlaceHolder>
            </div>
         <div class="buttons">
             <asp:Button ID="btnStay" runat="server" Text="Stay"  OnClick="btnStay_Click" />
             <asp:Button ID="btnHit" runat="server" Text="Hit"  OnClick="btnHit_Click" />
       
            <asp:PlaceHolder ID="playerButtons" Visible="false" runat="server">
                <h6>You have an Ace. How would you like to play it ? </h6>


                <asp:Button ID="btOnePoint" runat="server" UseSubmitBehavior="false" Text="1 point" CausesValidation="false" OnClick="btOnePoint_Click" />

                
                <asp:Button class="btn btn-primary" ID="btElevenPoints" runat="server" Text="11 points" OnClick="btElevenPoints_Click" />
            </asp:PlaceHolder>
        </div>
         <h3><%:Session["player"]%></h3>
    </div>
    </form>
</body>
</html>
