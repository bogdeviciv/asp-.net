﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BlackJack
{
   
    public partial class Main : System.Web.UI.Page
    {
        static Random rnd = new Random();
        Player player = new Player();
        Player dealer = new Player();

        public static string[,] deck = new string[,]{{"2C","2D","2H","2S"},{"3C","3D","3H","3S"},{"4C","4D","4H","4S"},{"5C","5D","5H","5S"},
            {"6C","6D","6H","6S"},{"7C","7D","7H","7S"},{"8C","8D","8H","8S"},{"9C","9D","9H","9S"},{"10C","10D","10H","10S"},
        {"JC","JD","JH","JS"},{"QC","QD","QH","QS"},{"KC","KD","KH","KS"},{"AC","AD","AH","AS"}};


      

        public string generateCard(Player p)
        {
           
            int i = rnd.Next(0, 13); // creates a number between 0 and 12
            int j = rnd.Next(0, 4);   // creates a number between 0 and 3

            while (deck[i, j].Equals(" "))
            {
                i = rnd.Next(0, 13); // creates a number between 0 and 12
                j = rnd.Next(0, 4);   // creates a number between 0 and 3

                
            }
           
            p.Cards.Add(i+2);
            if (i >= 0 && i <= 7)
            {
                p.Points += (i + 2);
            }
            else if(i>=8 && i <= 11)
            {
                p.Points += 10;
            }
            


            string card = deck[i, j];
            deck[i, j] = " ";
            return card;



        }

        public void ValidateUsersCards()
        {
            if(player.Cards[0]==14 || player.Cards[1] == 14)
            {
                // player has an ace
                playerButtons.Visible = true;

            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            /*Used to prevent page refresh from button clicks
            btnHit.Attributes.Add("onclick", "return false;");
            btnStay.Attributes.Add("onclick", "return false;");
            btOnePoint.Attributes.Add("onclick", "return false;");
            btElevenPoints.Attributes.Add("onclick", "return false;");
            */
            if (!IsPostBack) {

                Image card1 = new Image(); // instantiating the control
                string firstCard = generateCard(dealer);
                card1.ImageUrl = "img/red_back.png"; // setting the path to the image
                card1.Height = 200;
                card1.Width = 120;
                dealerHand.Controls.Add(card1);

                Image card2 = new Image(); // instantiating the control

                card2.ImageUrl = "img/" + generateCard(dealer) + ".png"; // setting the path to the image
                card2.Height = 200;
                card2.Width = 120;
                dealerHand.Controls.Add(card2);


                for (int i = 0; i < 2; i++)
                {
                    Image players_card = new Image(); // instantiating the control
                    players_card.ImageUrl = "img/" + generateCard(player) + ".png"; // setting the path to the image
                    players_card.Height = 200;
                    players_card.Width = 120;
                    playerHand.Controls.Add(players_card);
                }

                ValidateUsersCards();
            }
           

        }

        protected void btnStay_Click(object sender, EventArgs e)
        {
            btnHit.Visible = false;
        }

        protected void btnHit_Click(object sender, EventArgs e)
        {
            
        }

        protected void btOnePoint_Click(object sender, EventArgs e)
        {
            player.Points +=1;
        }

        protected void btElevenPoints_Click(object sender, EventArgs e)
        {
            player.Points +=11;
        }

    }
}