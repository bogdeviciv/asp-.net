﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlackJack
{
    public class Player
    {
        List<int> cards = new List<int>();
        int points;
        double cash;

        public int Points { get => points; set => points = value; }
        public List<int> Cards { get => cards; set => cards = value; }
        public double Wallet { get => cash; set => cash = value; }
    }
}